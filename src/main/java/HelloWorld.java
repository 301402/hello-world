import static spark.Spark.*;

public class HelloWorld {
    public static void main(String[] args) {
        staticFiles.location("/public");

        get("/", (req, res) -> {
            res.redirect("index.html");
            return 0;
        });
    }
}